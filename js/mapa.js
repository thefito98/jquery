var map = L.map('map').setView([51.505, -0.09], 2);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


L.marker([37.4030185,-122.3212903, 1]).addTo(map)
    .bindPopup('Silicon Valley')
    .openPopup();

L.marker([20.6739383,-103.4054539, 1]).addTo(map)
    .bindPopup('Guadalajara, México')
    .openPopup();

L.marker([25.6490376,-100.4431833, 1]).addTo(map)
    .bindPopup('Monterrey, México')
    .openPopup();

L.marker([19.3910038,-99.2837003, 1]).addTo(map)
    .bindPopup('CDMX')
    .openPopup();

L.marker([22.321061, 114.168380, 1]).addTo(map)
    .bindPopup('Hong Kong, China')
    .openPopup();

L.marker([19.8552836,-90.5285385, 1]).addTo(map)
    .bindPopup("McCarthy's Iris Pub, Campeche, México")
    .openPopup();